install:
	flatpak-builder --repo=repo --force-clean build rest.insomnia.Insomnia.yml
	flatpak remove rest.insomnia.Insomnia -y
	flatpak --user install tutorial-repo rest.insomnia.Insomnia -y
	flatpak run rest.insomnia.Insomnia

